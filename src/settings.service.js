import { stamp, slug } from './functions';
import { db } from './db.service';
import { ASubject } from './a.subject';

class Settings {

    async insertNew(name, type, value) {
        this.settings.data[name] = {
            name: name,
            type: type,
            value: value
        };
        return await db.run('INSERT INTO settings VALUES(?, ?, ?)', [name, type, value]);
    }
    
    async ensureSetting(name, type, value) {
        let row = await db.get('SELECT * FROM settings WHERE name = ?', [name]);
        if (!row) {
            await this.insertNew(name, type, value);
        } else if (row.type !== type) {
            await db.run('UPDATE settings SET type = ? WHERE name = ?', [type, name]);
        }
    }

    getValue(name) {
        let row = this.settings.data[name];
        if (row.type == 'integer') {
            return parseInt(row.value);
        }
        if (row.type == 'float') {
            return parseFloat(row.value);
        }
        return row.value;
    }

    getObject(name) {
        return this.settings.data[name];
    }

    async setValue(name, value) {
        await db.run('UPDATE settings SET value = ? WHERE name = ?', [value, name]);
        this.settings.data[name].value = value;
        this.settings.next(this.settings.data);
    }

    getNames() {
        return Object.keys(this.settings.data);
    }

    getAllSettings() {
        return Object.assign({}, this.settings.data);
    }

    constructor() {
        let sql = 'CREATE TABLE IF NOT EXISTS settings(' +
            'name TEXT' +
            ', type TEXT' +
            ', value TEXT' +
            ')';

        this.settings = new ASubject();
        this.settings.data = {};
        
        db.run(sql, []).then( async () => {
            //on application run we will recreate settings if needed, or fix the type
            await this.ensureSetting('API_URL', 'string', 'https://xxxxcompanyxxxx.atlassian.net/');
            await this.ensureSetting('LIST_OF_PROJECTS', 'text', 'NEXT,NMGG,VMO,VMM3,VM,BI,DIG,MP,DG,VMC,SEOCRO,WL,DHM,DPR');
            await this.ensureSetting('CUSTOM_TASKS', 'text', 'Daily_call,Skype_interrupt,Owner_called,Emails,Emergency,Help_for_devs');
            await this.ensureSetting('JIRA_USERNAME', 'string', 'andrey.bezpalenko1');
            await this.ensureSetting('JIRA_PASSWORD', 'password', 'xxx123');
            await this.ensureSetting('TIME_OFFSET', 'string', '09:35');
            await this.ensureSetting('COMPANY_TIME_OFFSET', 'string', '+2');
            await this.ensureSetting('SKYPE_TICKET', 'string', 'DG-32');
            await this.ensureSetting('ADHOC_TICKET', 'string', 'DG-2');
            await this.ensureSetting('JIRA_SEARCH_TO_WORK', 'text', 'assignee = currentUser() ' +
                'AND project != DG ' +
                'AND (status in ("Selected For Development", "In Progress") ' +
                'OR (status in ("Ready For Dev", "Dev In Progress") AND project = DPR)) ' +
                'ORDER BY updated DESC');

            //now load all settings, because we don't need to wait for db, every time we need some setting
            let settings = {};
            let rows = await db.all('SELECT * FROM settings', []);
            for (let row of rows) {
                settings[row.name] = row;
            }
            this.settings.next(settings);
        });
    }
}

export let settings = new Settings();

