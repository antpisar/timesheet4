require('../less/main.less');
'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import {MainWindow} from './components';

window.rerender = function() {
    ReactDOM.render(
        <MainWindow />
    ,
        document.getElementById('root')
    );
};

window.rerender();
setInterval(() => {window.rerender();}, 5000);