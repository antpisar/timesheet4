const { shell, ipcRenderer } = require('electron');

import React from 'react';
import { tasks } from './tasks.service';
import {toTime,toDate,stamp,spentFormat,spentToSeconds, validateSpent} from './functions';
import { ModalWindows, modalsControl, ModalData } from './modals';
import { settings } from './settings.service';
import { apiService } from './api.service';


export class MainWindow extends React.Component {

    constructor(props) {
        super(props);
        this.state = {curTask: '', tasks: []};
        this.form = null;
        tasks.currentTask.subscribe((v) => {
            let state = Object.assign({}, this.state);
            state.curTask = v;
            this.setState(state);
        });
        settings.settings.subscribe((data) => {
            if (data.CUSTOM_TASKS) {
                let state = Object.assign({}, this.state);
                state.tasks = data.CUSTOM_TASKS.value.split(',');
                this.setState(state);
            }
        });

    }

    startTask() {
        if (this.form.state.curTaskName && this.form.state.curTaskName.match(/\w+/)) {

            let value = this.form.state.curTaskName.replace(/^\s+|\s+$/g, '');
            this.form.setState({curTaskName: value});

            tasks.startTask(value);
        }
    }

    stopTask() {
        tasks.stopTask();
    }

    className() {
        return "main-window " + (this.state.curTask ? 'green' : '')
    }

    onCallClick(e) {
        modalsControl.showModal('HadACallModal', {callback: (text, seconds) => {
            tasks.replaceLastMinutes(text, seconds);
        }});
    }

    onSettingsClick(e) {
        modalsControl.showModal('SettingsModal');
    }

    onReportClick(e) {
        modalsControl.showModal('ReportModal');
    }

    onMonthReportClick(e) {
        modalsControl.showModal('MonthReportModal');
    }

    onDebugClick(e) {
        ipcRenderer.send('enable-debug');
    }

    onCustomTicket(e) {
        modalsControl.showModal('AddCustomTaskModal', {callback: (name, startAt, stopAt) => {
            tasks.addNewTask(name, startAt, stopAt);
        }});
    }

    render() {

        let quickTasks = this.state.tasks.map((name, i) => {
            return (
                <StartTask key={i} name={name}/>
            );
        });

        return (
        <div className={this.className()}>
            <ModalWindows />
            <h6>vis tracker 4 (electron + react + sqlite)</h6>
            <div className="settings-icon" onClick={(e) => this.onSettingsClick(e)}>&#9881;</div>
            <div className="report-icon" onClick={(e) => this.onReportClick(e)}>&#9997;</div>
            <div className="month-report-icon" onClick={(e) => this.onMonthReportClick(e)}>&#x1F58A;&#xFE0F;</div>
            <div className="debug-icon" onClick={(e) => this.onDebugClick(e)}>&#x1F41E;</div>
            <div>
                <NewTaskForm ref={(ref) => {this.form = ref;}}/>
                <button onClick={(e) => this.startTask(e)} >START</button>
                <button onClick={(e) => this.stopTask(e)} >STOP</button>
            </div>
            <div>
                {quickTasks}
                <button onClick={(e) => this.onCallClick(e)}>IhadAcall</button>
                <button onClick={(e) => this.onCustomTicket(e)}>CUSTOM</button>
            </div>
            <SelectedTask/>
            <LastDaysList/>
        </div>
        );
    }
}

class StartTask extends React.Component {

    constructor(props) {
        super(props);
    }

    onClick(e, name) {
        tasks.continueTask(name);
    }

    render() {
        return (
            <div className="quick-start href" onClick={(e) => this.onClick(e, this.props.name)}>
                {this.props.name}
            </div>
        );
    }
}

class SelectedTask extends React.Component {

    constructor(props) {
        super(props);
        this.state = {curTaskName: ''};

        tasks.currentTask.subscribe((v) => {
            this.setState({curTaskName: v});
        });
    }

    render() {
        return (
            <div className="task-name">
                Current: {this.state.curTaskName}
            </div>
        );
    }
}

class NewTaskForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {curTaskName: ''};

        tasks.currentTask.subscribe((v) => {
            this.setState({curTaskName: v});
        });
    }

    handleChange(event) {
        this.setState({curTaskName: event.target.value});
    }
    handleKeyDown(e) {
        if (e.keyCode === 13) {
            tasks.startTask(this.state.curTaskName);
        }
    }

    render() {
        return (
            <input className="new-task"
                   value={this.state.curTaskName}
                   onChange={(e) => this.handleChange(e)}
                   onKeyDown={(e) => this.handleKeyDown(e)}
            />
        );
    }
}


class LastDaysList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {days: []};

        tasks.currentTask.subscribe((v) => {
            let state = Object.assign({}, this.state);
            state.curTask = v;
            this.setState(state);

            this.reloadDays();
        });
        tasks.days.subscribe((days) => {
            let state = Object.assign({}, this.state);
            state.days = days;
            this.setState(state);
        });
    }

    reloadDays() {
        tasks.loadDays().then((days) => {
            this.setState({days: days});
        });
    }

    render() {

        let days = this.state.days.map((day, number) => {
           let tasksL = day.tasks;
           let sumTime = 0;
           tasksL.forEach((task) => {
               sumTime += task.seconds;
           });
           return (
             <div key={number.toString()} className="day">
                 <b>{day.name}</b> <i>{spentFormat(sumTime)}</i>
                 <TaskList key={number.toString()} tasks={tasksL} curTask={this.state.curTask}/>
             </div>
           );
        });

        return (
            <div>
                Last days:
                {days}
            </div>
        );
    }
}

class TaskList extends React.Component {

    constructor(props) {
        super(props);
    }

    delete(e, rowid) {
        tasks.deleteTask(rowid);
    }

    continue(e, name) {
        tasks.continueTask(name);
    }

    async onClickWorkLog(e, t) {

        // Collect all the same at first
        let allTasks = await tasks.findSimilarNotLoggedTasks(t.name, t.startAt);
        if (allTasks.length < 1) {
            console.error('Should be at least 1 task with this name', t);
            return;
        }

        let task = allTasks[0];
        let seconds = 0;

        allTasks.forEach((t) => {
            seconds += t.seconds;
        });

        let name = task.name + ', time: ' + spentFormat(seconds);
        if (allTasks.length > 1) {
            name += `, ${allTasks.length} records included`;
        }

        if (task.ticket) {
            modalsControl.showModal('AddWorkLogModal', new ModalData(true, async (comment, remaining) => {

                let name = task.name + ' /// ' + comment;

                for (let t of allTasks) {
                    if (t === task) {
                        continue;
                    }
                    t.seconds = 0;
                    t.logged = 1;
                    t.name = name; //updating name here
                    await tasks.updateLoggedTask(t.rowid, t.seconds, t.name);
                }

                task.seconds = seconds;
                task.name = name; //updating name here
                task.logged = 1;
                await tasks.updateLoggedTask(task.rowid, task.seconds, task.name);

                await tasks.logTask(task.rowid, comment, remaining);

            }, name, '', ''));
        } else {
            modalsControl.showModal('AddWorkLogOtherModal', new ModalData(true, async (type) => {

                for (let t of allTasks) {
                    if (t === task) {
                        continue;
                    }
                    t.seconds = 0;
                    t.logged = 1;
                    await tasks.updateLoggedTask(t.rowid, t.seconds, t.name);
                }

                task.seconds = seconds;
                task.logged = 1;
                await tasks.updateLoggedTask(task.rowid, task.seconds, task.name);

                if (type == 'skype') {
                    await tasks.logTask(task.rowid, task.name, null, settings.getValue('SKYPE_TICKET'));
                } else if (type == 'adhoc') {
                    await tasks.logTask(task.rowid, task.name, null, settings.getValue('ADHOC_TICKET'));
                }

            }, name, '', ''));
        }
    }

    onClickName(e, task) {
        if (task.logged) {
            var range = document.createRange();
            range.selectNode(e.target);
            window.getSelection().removeAllRanges();
            window.getSelection().addRange(range);
            return;
        }
        modalsControl.showModal('EditDescModal', new ModalData(true, (newName) => {
            tasks.rename(task.rowid, newName);
        }, '', task.name));
    }

    onClickStartAt(e, task) {
        if (task.logged) {
            return;
        }
        modalsControl.showModal('EditTimeModal', new ModalData(true, (newTime) => {
            tasks.updateStartAt(task.rowid, newTime);
        }, '', task.startAt, task.name + ' Start At'));
    }

    onClickStopAt(e, task) {
        if (task.logged) {
            return;
        }
        modalsControl.showModal('EditTimeModal', new ModalData(true, (newTime) => {
            tasks.updateStopAt(task.rowid, newTime);
        }, '', task.stopAt, task.name + ' Stop At'));
    }

    onClickLink(e, task) {
        let href = apiService.generateLink(task.ticket);
        if (href.match(/^http.*/)) {
            shell.openExternal(href);
        }
    }

    renderButtonJira(task) {
        if (task.logged) {
            return (<div><b>Logged</b></div>);
        }
        let isCurrentTask = this.props.curTask && (this.props.curTask === task.name);
        if (isCurrentTask) {
            return (<span>cur task</span>);
        }
        return (
            <button onClick={(e) => this.onClickWorkLog(e, task)}>JIRA</button>
        );
    }

    renderButtonDelete(task) {
        if (task.logged) {
            return '';
        }
        return (
            <button onClick={(e) => this.delete(e, task.rowid)}>RM</button>
        );
    }

    renderStopAt(task) {
        if (task.logged) {
            return '';
        }
        return (
            <div className="stopAt" onClick={(e) => this.onClickStopAt(e, task)}>stop: {toTime(task.stopAt)}</div>
        );
    }

    render() {
        let timeNow = stamp(new Date());
        let tList = this.props.tasks.map((t, i) => {

            if (!t.stopAt) {
                t.seconds = timeNow - t.startAt;
            }

            return (
              <div key={i.toString()} className={'task' + (t.logged ? ' logged' : '')  + (t.seconds === 0 ? ' empty' : '') }>
                  <div className="buttons">
                      {this.renderButtonJira(t)}
                      {this.renderButtonDelete(t)}
                    <button onClick={(e) => this.continue(e, t.name)}>START</button>
                  </div>
                  <div className="name" onClick={(e) => this.onClickName(e, t)}>{t.name}</div>
                  <div className="seconds">{spentFormat(t.seconds)}</div>
                  <div className="startAt" onClick={(e) => this.onClickStartAt(e, t)}>start: {toTime(t.startAt)}</div>
                  {this.renderStopAt(t)}
                  <div className="link" onClick={(e) => this.onClickLink(e, t)}>{apiService.generateLink(t.ticket)}</div>
              </div>
            );
        });

        return (
            <div className="tasks-list">
              {tList}
            </div>
        );
    }
}



