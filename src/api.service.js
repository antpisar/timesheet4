import { JiraAPI } from './jira.api';
import { settings } from './settings.service';

class APIService {

    constructor() {
        this.api = null;
        settings.settings.subscribe((data) => {
            if (data.API_URL && data.API_URL.value) {
                this.setIntegrationURL(data.API_URL.value, data.JIRA_USERNAME.value, data.JIRA_PASSWORD.value, data.JIRA_SEARCH_TO_WORK.value);
            }
        });
    }

    generateLink(ticket) {
        if (!this.api) {
            return '';
        }
        return this.api.generateLink(ticket);
    }

    async getTicketSummary(ticket) {
        if (!this.api || !ticket) {
            return '';
        }
        return await this.api.getTicketSummary(ticket);
    }

    async postWorkLog(ticket, startAt, seconds, description, remaining) {
        if (!this.api) {
            return '';
        }
        return await this.api.postWorkLog(ticket, startAt, seconds, description, remaining);
    }

    async loadWorkLogsBetweenDates(startAt, stopAt) {
        if (!this.api) {
            return [];
        }
        return await this.api.loadWorkLogsBetweenDates(startAt, stopAt);
    }

    async loadToWorkTickets() {
        if (!this.api) {
            return [];
        }
        return await this.api.loadToWorkTickets();
    }

    setIntegrationURL(url, username, password, queryToWork) {
        if (JiraAPI.support(url)) {
            this.api = new JiraAPI(url, username, password, queryToWork);
        }
    }
}

export let apiService = new APIService();