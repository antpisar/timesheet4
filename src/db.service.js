const sqlite3 = require('sqlite3').verbose();
const fs = require('fs');
const os = require('os');

let home = os.homedir();

if (!fs.existsSync(home + '/.timesheet4')) {
    fs.mkdirSync(home + '/.timesheet4');
}
if (!fs.existsSync(home + '/.timesheet4/tasks.db')) {
    fs.writeFileSync(home + '/.timesheet4/tasks.db', '');
}
let sqliteDb = new sqlite3.Database(home + '/.timesheet4/tasks.db', (err) => {
    if (err) {
        return console.error(err.message);
    }
});

class DatabaseService {

    run(sql, params) {
        return new Promise((resolve, reject) => {
            sqliteDb.run(sql, params, function (err) {
                if (err) {
                    throw err;
                    //reject(err);
                    //return;
                }
                resolve(this.lastID);
            });
        });
    }

    get(sql, params) {
        return new Promise((resolve, reject) => {
            sqliteDb.get(sql, params, function (err, row) {
                if (err) {
                    throw err;
                    //reject(err);
                    //return;
                }
                resolve(row);
            });
        });
    }

    all(sql, params) {
        return new Promise((resolve, reject) => {
            sqliteDb.all(sql, params, function (err, rows) {
                if (err) {
                    throw err;
                    //reject(err);
                    //return;
                }
                resolve(rows);
            });
        });
    }
}

export let db = new DatabaseService();