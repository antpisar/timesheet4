import React from 'react';
import { ASubject } from './a.subject';
import { toTime, toDate, stamp, spentFormat, spentToSeconds, validateSpent, datetime, datetimeDate, dateTimeWeekDay, dateTimeDateAndWeek, slug } from './functions';
import { settings } from './settings.service';
import { apiService } from './api.service';
import { tasks } from './tasks.service';

export class ModalData {
    constructor(visible, callback, name, value, additional) {
        this.visible = visible;
        this.callback = callback;
        this.name = name;
        this.value = value;
        this.additional = additional;
    }
}

class ModalsControl {
    constructor() {
        this.modals = {};
        this.modalsSubject = new ASubject();
    }

    showModal(name, modalData) {
        modalData = modalData || new ModalData(false, null);
        modalData.visible = true;
        this.modals[name] = modalData;
        this.modalsSubject.next(this.modals);
    }

    hideModal(name) {
        this.modals[name].visible = false;
        this.modalsSubject.next(this.modals);
    }
}

export class ModalWindows extends React.Component {

    render() {
        return (
            <div className="modals i">
                <HadACallModal />
                <SettingsModal />
                <EditDescModal />
                <EditTimeModal />
                <AddCustomTaskModal />
                <AddWorkLogModal />
                <AddWorkLogOtherModal />
                <ReportModal />
                <MonthReportModal />
            </div>
        );
    }
}

class HadACallModal extends React.Component {

    name() {
        return 'HadACallModal';
    }

    constructor(props) {
        super(props);
        this.state = {
            modal: new ModalData(false, null, null),
            seconds: 0,
            desc: '',
            timeString: ''
        };

        modalsControl.modalsSubject.subscribe((data) => {

            if (data[this.name()]) {
                let v = data[this.name()];
                this.setState({
                    modal: v,
                    seconds: 0,
                    desc: '',
                    timeString: ''
                });
            }
        });
    }

    onTextChange(e) {
        this.setState({
            modal: this.state.modal,
            seconds: this.state.seconds,
            desc: e.target.value,
            timeString: this.state.timeString
        });
    }

    onButtonClick(e) {
        if (!this.state.seconds) {
            return;
        }
        if (this.state.modal.callback) {
            this.state.modal.callback(this.state.desc, this.state.seconds);
        }
        modalsControl.hideModal(this.name());
    }

    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    getSeconds() {
        return this.state.timeString;
    }

    onSecondsChange(e) {
        if (validateSpent(e.target.value)) {
            this.setState({
                modal: this.state.modal,
                seconds: spentToSeconds(e.target.value),
                desc: this.state.desc,
                timeString: e.target.value
            });
        }
    }

    render() {
        return (
            <div className={"modal had-a-call " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>I had a call</h3>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        Description:
                        <input type="text" placeholder="Description" onChange={(e)=>this.onTextChange(e)}/>
                    </div>
                    <div>
                        How long you had a call?
                        <input type="text" placeholder="Time like 2h 12m 3s" onChange={(e)=>this.onSecondsChange(e)} value={this.getSeconds()}/>
                    </div>
                    <div>
                        <button onClick={(e)=>this.onButtonClick(e)}>Insert or replace</button>
                    </div>
                </div>

            </div>
        );
    }
}

class SettingsModal extends React.Component {

    name() {
        return 'SettingsModal';
    }
    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    constructor(props) {
        super(props);
        this.state = {
            modal: new ModalData(false, null, null)
        };

        modalsControl.modalsSubject.subscribe((data) => {

            if (data[this.name()]) {
                let state = Object.assign({}, this.state);
                state.modal = data[this.name()];
                this.setState(state);
            }
        });
    }

    updateValue(e, setting) {
        settings.setValue(setting.name, e.target.value);
    }

    renderInput(setting) {
        if (setting.type == 'text') {
            return (<textarea defaultValue={setting.value}
                              onChange={(e) => this.updateValue(e, setting)}
                              onKeyUp={(e) => this.updateValue(e, setting)}></textarea>);
        }
        if (setting.type == 'password') {
            return (<input type="password" defaultValue={setting.value}
                           onChange={(e) => this.updateValue(e, setting)}
                           onKeyUp={(e) => this.updateValue(e, setting)} />);
        }
        return (<input type="text" defaultValue={setting.value}
                       onChange={(e) => this.updateValue(e, setting)}
                       onKeyUp={(e) => this.updateValue(e, setting)} />);
    }

    render() {

        let sList = settings.getNames().map((name, i) => {
            let setting = settings.getObject(name);

            return (
                <div key={i} className="setting">
                    <div className="name">{setting.name}</div>
                    <div className="value">
                        {this.renderInput(setting)}
                    </div>
                </div>
            );
        });

        return (
            <div className={"modal settings-modal " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>Settings</h3>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        {sList}
                    </div>
                </div>
            </div>
        );
    }
}

class MonthReportModal extends React.Component {
    name() {
        return 'MonthReportModal';
    }
    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    constructor(props) {
        super(props);
        this.state = {
            modal: new ModalData(false, null, null),
            date: '',
            report: ''
        };
        this.settings = {};
        settings.settings.subscribe((data) => {
            this.settings = data;
        });

        modalsControl.modalsSubject.subscribe((data) => {
            if (data[this.name()]) {
                let state = Object.assign({}, this.state);
                state.modal = data[this.name()];
                this.setState(state);
            }
        });
    }

    getWeekNumber(d) {
        // Copy date so don't modify original
        d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
        // Set to nearest Thursday: current date + 4 - current day number
        // Make Sunday's day number 7
        d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
        // Get first day of year
        var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
        // Calculate full weeks to nearest Thursday
        var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
        // Return array of year and week number
        return [d.getUTCFullYear(), weekNo];
    }

    onButtonClick(e) {
        //get a list of all ticket during the month
        let dateEnd = new Date();
        let dateStart = new Date();
        dateStart.setDate(dateStart.getDate() - 40);

        let state = Object.assign({}, this.state);
        state.report = "Loading, wait about 1 minute...";
        this.setState(state);

        setTimeout(async () => {
            let report = '';

            let days = await tasks.loadDaysBetween(dateStart, dateEnd);
            for (let day of days) {
                report += '\n';
                report += 'DAY: ' + day.name + ' - week: ' + this.getWeekNumber(day.dateObj) + '\n';

                for (let task of day.tasks) {
                    report += '-- ' + task.name + ' -- ' + spentFormat(task.seconds) + '\n';
                }
                report += '\n';
            }

            let state = Object.assign({}, this.state);
            state.report = report;
            this.setState(state);
        }, 500);
    }

    render() {

        return (
            <div className={"modal report-modal " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>Report for the last month</h3>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        <button onClick={(e) => this.onButtonClick(e)} > Generate report </button>
                    </div>
                    <textarea className="report-view" value={this.state.report} />
                </div>
            </div>
        );
    }
}


class ReportModal extends React.Component {

    name() {
        return 'ReportModal';
    }
    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    constructor(props) {
        super(props);
        this.state = {
            modal: new ModalData(false, null, null),
            date: '',
            report: ''
        };
        this.settings = {};
        settings.settings.subscribe((data) => {
            this.settings = data;
        });

        modalsControl.modalsSubject.subscribe((data) => {

            if (data[this.name()]) {
                let state = Object.assign({}, this.state);
                state.modal = data[this.name()];
                let d = new Date();
                if (d.getDay() == 1) {
                    d.setDate(d.getDate() - 3);
                } else if (d.getDay() == 0) {
                    d.setDate(d.getDate() - 2);
                } else {
                    d.setDate(d.getDate() - 1);
                }
                state.date = datetimeDate(d);
                this.setState(state);
            }
        });
    }

    // updateValue(e, setting) {
    //     settings.setValue(setting.name, e.target.value);
    // }

    changeDate(e) {
        let state = Object.assign({}, this.state);
        state.date = e.target.value;
        this.setState(state);
    }

    onButtonClick(e) {
        //collect all tickets and worklogs at first
        
        let dateEnd = new Date();
        const offsetMinutes = -dateEnd.getTimezoneOffset();
        const companyOffset = +(settings.getValue('COMPANY_TIME_OFFSET'));
        const CETOffset = companyOffset - offsetMinutes / 60;
        dateEnd.setHours(0, 0, 0, 0);
        dateEnd.setTime(dateEnd.getTime() + tasks.offsetSeconds * 1000);

        let dateStart = new Date(this.state.date);
        

        let state = Object.assign({}, this.state);
        state.report = "Loading, wait about 1 minute...";
        this.setState(state);

        let adHocPrefix = this.settings['ADHOC_TICKET'].value.split('-')[0];
        //console.log('adHocPrefix', adHocPrefix);

        setTimeout(async () => {
            //let workLogs = await apiService.loadWorkLogsBetweenDates(stamp(dateStart), stamp(dateEnd));
            let tickets = await apiService.loadToWorkTickets();

            // let workLogsByIssue = new Map();
            // for (let item of workLogs) {
            //     if (item['issue'].replace(/^(\w+)-\d+/, '$1') === adHocPrefix) {
            //         workLogsByIssue.set(item['issue'] + '-' + item['comment'], item);
            //     } else {
            //         if (workLogsByIssue.has(item['issue'])) {
            //             let obj = workLogsByIssue.get(item['issue']);
            //             obj['comment'] = obj['comment'] + ', ' + item['comment'];
            //             obj['worked'] += +item['worked'];
            //         } else {
            //             workLogsByIssue.set(item['issue'], item);
            //         }
            //     }
            // }

            // cycle by time
            dateStart.setHours(0, 0, 0, 0);
            dateStart.setTime(dateStart.getTime() + tasks.offsetSeconds * 1000);
            let listOfTasks = await tasks.loadTasksBetween(dateStart, dateEnd);
            listOfTasks = listOfTasks.reverse();
            listOfTasks = listOfTasks.filter(function(task){
                return task.stopAt !== null 
                        && task.stopAt !== 0 
                        && task.stopAt > task.startAt 
                        && task.stopAt - task.startAt >= 60;
            });

            let taskBySlugWorkedTotal = {};
            let tasksUnique = [];
            let ticketObjects = {}; //loaded from server

            for (let task of listOfTasks) {
                
                if (task.ticket) {
                    if (ticketObjects[task.ticket] === undefined) {
                        ticketObjects[task.ticket] = await apiService.getTicketSummary(task.ticket);
                    }
                    task.slug = slug(ticketObjects[task.ticket]['summary']);
                    task.name = ticketObjects[task.ticket]['summary'];
                }

                if (!taskBySlugWorkedTotal[task.slug]) {
                    taskBySlugWorkedTotal[task.slug] = 0;
                    tasksUnique.push(task);
                }
                task.seconds = task.stopAt - task.startAt;
                taskBySlugWorkedTotal[task.slug] += task.seconds;
            }

            let timeTotal = 0;
            for (let task of listOfTasks) {
                timeTotal += task.seconds;
            }


            // collapse identical tasks that goes after each other
            let i = 1;
            while (i < listOfTasks.length) {
                let j = i - 1;
                let task = listOfTasks[i];
                let prevTask = listOfTasks[j];
                if (task.slug == prevTask.slug &&  Math.abs(task.startAt-prevTask.stopAt) < 300) {
                    prevTask.stopAt = task.stopAt;
                    prevTask.seconds = prevTask.stopAt - prevTask.startAt;
                    task.seconds = 0;
                    listOfTasks.splice(i, 1);
                    continue;
                }
                i++;
            }

            let timeTotalText = spentFormat(timeTotal);
            let report = "Hello! \n\n1. I worked " + timeTotalText + " on:";

            for (let task of tasksUnique) {
                let total = taskBySlugWorkedTotal[task.slug];
                if (total < 60) {
                    continue;
                }
                report += "\n  - ";
                
                let ticket = task.ticket;

                let line = '';
                if (ticket && ticket.replace(/^(\w+)-\d+/, '$1') !== adHocPrefix) {
                    line = task.name + ' - ' + apiService.generateLink(ticket) + ' - ' 
                        + ticketObjects[ticket].status 
                        + ' - ' + spentFormat(total);
                } else {
                    line = task.name + ' - ' + spentFormat(total);
                }

                report += line;
            }

            report += "\n\n2. Plan to work on:\n";

            
            report += "\n";
            report += "\n";
            report += "\n";

            report += "\n\n3. Blockers: \n- Nothing blocking";

            report += "\n\n\n SAMPLES: \n\n";

            for (let item of tickets) {

                let dateEnd = new Date();
                dateEnd.setHours(0, 0, 0, 0);
                let eta = item['remaining'];
                let daysPlus = Math.floor(Math.ceil(eta / 5) - 1);
                daysPlus = Math.max(0, daysPlus);
                dateEnd.setDate(dateEnd.getDate() + daysPlus);

                report += `\n   - ${item['summary']} - ${item['link']} - ETA: ${item['remaining']}h, TO WORK: 000 h`;
            }

            let state = Object.assign({}, this.state);
            state.report = report;
            this.setState(state);
        }, 500);
    }

    render() {

        return (
            <div className={"modal report-modal " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>Report for a meeting:</h3>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        I set the start date for you, but you can change it:
                    </div>
                    <div>
                        <input type="text"
                               placeholder="Date like: Apr 12 2018"
                               value={this.state.date}
                               onChange={(e) => this.changeDate(e)}
                        />
                    </div>
                    <div>
                        <button onClick={(e) => this.onButtonClick(e)} > Generate report </button>
                    </div>
                    <textarea className="report-view" value={this.state.report} />
                </div>
            </div>
        );
    }
}

class EditDescModal extends React.Component {

    name() {
        return 'EditDescModal';
    }
    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    constructor(props) {
        super(props);
        this.state = {
            modal: new ModalData(false, null, null, '')
        };
        this.oldValue = '';

        modalsControl.modalsSubject.subscribe((data) => {

            if (data[this.name()]) {
                let state = Object.assign({}, this.state);
                state.modal = data[this.name()];
                this.oldValue = state.modal.value;
                this.setState(state);
            }
        });
    }

    updateValue(e) {
        let state = Object.assign({}, this.state);
        state.modal.value = e.target.value;
        this.setState(state);
    }

    onButtonClick(e) {
        let callback = this.state.modal.callback;
        if (callback) {
            callback(this.state.modal.value);
        }
        modalsControl.hideModal(this.name());
    }

    render() {

        return (
            <div className={"modal edit-desc-modal " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>Edit description of:</h3>
                    <h4>{this.oldValue}</h4>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        <input type="text" value={this.state.modal.value}
                               onChange={(e) => this.updateValue(e)}
                               onKeyUp={(e) => this.updateValue(e)} />
                    </div>
                    <div>
                        <button onClick={(e)=>this.onButtonClick(e)}>Update</button>
                    </div>
                </div>
            </div>
        );
    }
}

class EditTimeModal extends React.Component {

    name() {
        return 'EditTimeModal';
    }
    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    constructor(props) {
        super(props);
        this.state = {
            modal: new ModalData(false, null, '', '', ''),
            time: ''
        };

        modalsControl.modalsSubject.subscribe((data) => {

            if (data[this.name()]) {
                let state = Object.assign({}, this.state);
                state.modal = data[this.name()];
                state.time = this.getStringValue(state.modal.value);
                this.setState(state);
            }
        });
    }

    updateValue(e) {
        let state = Object.assign({}, this.state);
        state.time = e.target.value;
        this.setState(state);
    }

    onButtonClick(e) {
        let callback = this.state.modal.callback;

        if (this.state.time === '') {
            callback(0);
        } else {
            let yearAgo = new Date();
            yearAgo.setFullYear(yearAgo.getFullYear() - 1);

            let newDate = new Date(this.state.time);
            if (isNaN(newDate.getTime())) {
                return;
            }
            if (newDate.getTime() < yearAgo.getTime()) {
                return;
            }
            callback(stamp(newDate));
        }

        modalsControl.hideModal(this.name());
    }

    getStringValue(time) {
        let d = new Date();
        d.setTime(time * 1000);
        return datetime(d);
    }

    render() {

        return (
            <div className={"modal edit-time-modal " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>Edit {this.state.modal.additional} time</h3>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        <input type="text" value={this.state.time}
                               onChange={(e) => this.updateValue(e)}
                               onKeyUp={(e) => this.updateValue(e)} />
                    </div>
                    <div>
                        <button onClick={(e)=>this.onButtonClick(e)}>Update</button>
                    </div>
                </div>
            </div>
        );
    }
}

class AddCustomTaskModal extends React.Component {

    name() {
        return 'AddCustomTaskModal';
    }
    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    createState() {
        return {
            modal: new ModalData(false, null, '', '', ''),
            name: '',
            startAt: datetime(new Date()),
            stopAt: datetime(new Date())
        }
    }

    constructor(props) {
        super(props);
        this.state = this.createState();

        modalsControl.modalsSubject.subscribe((data) => {

            if (data[this.name()]) {
                let state = this.createState();
                state.modal = data[this.name()];

                this.setState(state);
            }
        });
    }

    updateName(e) {
        let state = Object.assign({}, this.state);
        state.name = e.target.value;
        this.setState(state);
    }

    updateStartAt(e) {
        let state = Object.assign({}, this.state);
        state.startAt = e.target.value;
        this.setState(state);
    }

    updateStopAt(e) {
        let state = Object.assign({}, this.state);
        state.stopAt = e.target.value;
        this.setState(state);
    }

    onButtonClick(e) {
        let callback = this.state.modal.callback;
        if (callback) {

            let startAt = new Date(this.state.startAt);
            let stopAt = new Date(this.state.stopAt);
            if (isNaN(startAt.getTime())) {
                return;
            }

            callback(
                this.state.name,
                stamp(startAt),
                !isNaN(stopAt.getTime()) ? stamp(stopAt) : 0
            );
        }
        modalsControl.hideModal(this.name());
    }

    render() {

        return (
            <div className={"modal add-custom-modal " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>Creating new custom task:</h3>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        Name:
                        <input type="text" value={this.state.name}
                               onChange={(e) => this.updateName(e)}
                               onKeyUp={(e) => this.updateName(e)} />
                    </div>
                    <div>
                        StartAt:
                        <input type="text" value={this.state.startAt}
                               onChange={(e) => this.updateStartAt(e)}
                               onKeyUp={(e) => this.updateStartAt(e)} />
                    </div>
                    <div>
                        StopAt:
                        <input type="text" value={this.state.stopAt}
                               onChange={(e) => this.updateStopAt(e)}
                               onKeyUp={(e) => this.updateStopAt(e)} />
                    </div>
                    <div>
                        <button onClick={(e)=>this.onButtonClick(e)}>Insert</button>
                    </div>
                </div>
            </div>
        );
    }
}

class AddWorkLogModal extends React.Component {

    name() {
        return 'AddWorkLogModal';
    }
    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    createState() {
        return {
            modal: new ModalData(false, null, '', '', ''),
            comment: '',
            remaining: '',
        }
    }

    constructor(props) {
        super(props);
        this.state = this.createState();
        this.input = null;

        modalsControl.modalsSubject.subscribe((data) => {

            if (data[this.name()]) {
                let state = this.createState();
                state.modal = data[this.name()];

                this.setState(state);
            }
            if (this.input) {
                this.input.focus();
            }
        });
    }

    updateComment(e) {
        let state = Object.assign({}, this.state);
        state.comment = e.target.value;
        this.setState(state);
        if (e.type && e.type === 'keyup') {
            if (e.keyCode === 13) {
                this.onButtonClick(e);
            }
            if (e.keyCode === 27) {
                this.onClose(e);
            }
        }
    }

    updateRemaining(e) {
        let state = Object.assign({}, this.state);
        state.remaining = e.target.value;
        this.setState(state);
        if (e.type && e.type === 'keyup') {
            if (e.keyCode === 13) {
                this.onButtonClick(e);
            }
            if (e.keyCode === 27) {
                this.onClose(e);
            }
        }
    }

    onButtonClick(e) {
        let callback = this.state.modal.callback;
        if (callback) {

            callback(
                this.state.comment,
                this.state.remaining
            );
        }
        modalsControl.hideModal(this.name());
    }

    render() {

        return (
            <div className={"modal add-work-log-modal " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>Add worklog:</h3>
                    <p> <b>Task</b> : {this.state.modal.name} </p>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        Comment:
                        <input type="text" value={this.state.comment}
                               ref={(ref) => {this.input = ref;}}
                               onChange={(e) => this.updateComment(e)}
                               onKeyUp={(e) => this.updateComment(e)} />
                    </div>
                    <div>
                        Remaning (example: 1h 2m or leave empty for automatic) :
                        <input type="text" value={this.state.remaining}
                               onChange={(e) => this.updateRemaining(e)}
                               onKeyUp={(e) => this.updateRemaining(e)} />
                    </div>
                    <div>
                        <button onClick={(e)=>this.onButtonClick(e)}>Insert</button>
                    </div>
                </div>
            </div>
        );
    }
}

class AddWorkLogOtherModal extends React.Component {

    name() {
        return 'AddWorkLogOtherModal';
    }
    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    createState() {
        return {
            modal: new ModalData(false, null, '', '', ''),
        }
    }

    constructor(props) {
        super(props);
        this.state = this.createState();

        modalsControl.modalsSubject.subscribe((data) => {

            if (data[this.name()]) {
                let state = this.createState();
                state.modal = data[this.name()];

                this.setState(state);
            }
        });
    }

    onButtonClick(type) {
        let callback = this.state.modal.callback;
        if (callback) {
            callback(type);
        }
        modalsControl.hideModal(this.name());
    }

    render() {

        return (
            <div className={"modal add-work-log-other-modal " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>Add other worklog:</h3>
                    <p> <b>Task</b> : {this.state.modal.name} </p>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        <button onClick={(e)=>this.onButtonClick('skype')}>Skype</button>
                    </div>
                    <div>
                        <button onClick={(e)=>this.onButtonClick('adhoc')}>AdHoc</button>
                    </div>
                </div>
            </div>
        );
    }
}

export let modalsControl = new ModalsControl();