
#Install dependencies
 
    sudo npm install -g electron 
    npm install
    npm run postinstall

# Prod usage

Now you can start it by running:

    npm run build
    npm start


# Dev usage
### Prepare dev
Check that you have in index.html script lines enabled:

    <script src="http://localhost:8080/webpack-dev-server.js"></script>
    <script src="http://localhost:8080/built/bundle.js"></script>

And prod.bundle.js commented (disabled).

Also in main.js uncomment these lines:

    // win = new BrowserWindow({width: 1024, height: 600})
    
    // win.webContents.openDevTools();
    
And comment this one:

    win = new BrowserWindow({width: 370, height: 600});

### Start dev
Run server from 2 terminals:

    npm run watch
    
And second:

    npm start
    

# Build project for prod use
    

    npm run build
    npm run dist


#### To create linux package: 

    node_modules/.bin/build -l

:)

#### Jira login
Now jira has deprecated old ways to login with Name and Password.
Now you have to use your Email and Token.
To create a new token go here:
https://id.atlassian.com/manage/api-tokens
and create one.
It will be used as password in this tracker app.

