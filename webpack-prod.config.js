var webpack = require('webpack');
const path = require('path');

module.exports = {
    entry: {
        app: path.resolve(__dirname, './src/entry.js')
    },
    output: {
        path: path.resolve(__dirname, "./public/built"),
        filename: 'prod.bundle.js',
        publicPath: path.resolve(__dirname, 'public')
    },
    module: {
        rules: [
            { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
            { test: /\.css$/, loader: 'style-loader!css-loader' },
            { test: /\.less$/, loader: 'style-loader!css-loader!less-loader'}
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ],
    mode: 'production',
    externals: {
        sqlite3: 'commonjs sqlite3',
        fs: 'commonjs fs',
        os: 'commonjs os',
        electron: 'commonjs electron',
        http: 'commonjs http',
        https: 'commonjs https',
        querystring: 'commonjs querystring'
    }
};